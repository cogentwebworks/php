## Small Latest PHP container but still have a working s6-overlay process and socklog-overlay


[![Docker Pulls](https://img.shields.io/docker/pulls/cogentwebs/php.svg)](https://hub.docker.com/r/cogentwebs/php/)
[![Docker Stars](https://img.shields.io/docker/stars/cogentwebs/php.svg)](https://hub.docker.com/r/cogentwebs/php/)
[![Docker Build](https://img.shields.io/docker/automated/cogentwebs/php.svg)](https://hub.docker.com/r/cogentwebs/php/)
[![Docker Build Status](https://img.shields.io/docker/build/cogentwebs/php.svg)](https://hub.docker.com/r/cogentwebs/php/)

This is a very small php container but still have a working s6-overlay process and  socklog-overlay . This is the complete php 7.2 AIO Extension 